Rails.application.routes.draw do
  resources :comments
  
  resources :contributions do
    put 'vote', on: :member
    put 'unvote', on: :member
  end
  
  resources :comments do
    put 'vote', on: :member
    put 'unvote', on: :member
  end
  
 
 
  resources :contributions do
    resources :comments
  end
  
  resources :users
  root 'contributions#index'
  get '/ask' => 'contributions#ask'
  
  get "/login", to: redirect("/auth/google_oauth2")
  
  get "/auth/google_oauth2/callback", to: "sessions#create"
  
  get "/logout", to:  "sessions#destroy"
  
  get "/contributions_user", to: "contributions#contributions_user"
  
  get "/comments_user", to: "comments#comments_user"

  get "/newest", to: "contributions#newest"
  resource :session, only: [:create, :destroy]
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
