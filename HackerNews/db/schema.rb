# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_05_13_175842) do

  create_table "comments", force: :cascade do |t|
    t.integer "user_id"
    t.text "text"
    t.integer "contribution_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "parent_id"
    t.integer "points"
  end

  create_table "contributions", force: :cascade do |t|
    t.string "title"
    t.string "url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "ask"
    t.integer "points"
    t.boolean "type_url"
    t.integer "user_id"
    t.index ["user_id"], name: "index_contributions_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "about"
    t.string "APIKey"
    t.index ["APIKey"], name: "index_users_on_APIKey", unique: true
  end

  create_table "votes", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id", null: false
    t.integer "contribution_id", null: false
    t.integer "comment_id"
    t.index ["comment_id"], name: "index_votes_on_comment_id"
    t.index ["contribution_id"], name: "index_votes_on_contribution_id"
    t.index ["user_id"], name: "index_votes_on_user_id"
  end

  create_table "votes_comments", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "user_id", null: false
    t.integer "comment_id", null: false
    t.index ["comment_id"], name: "index_votes_comments_on_comment_id"
    t.index ["user_id"], name: "index_votes_comments_on_user_id"
  end

  add_foreign_key "contributions", "users"
  add_foreign_key "votes", "comments"
  add_foreign_key "votes", "contributions"
  add_foreign_key "votes", "users"
  add_foreign_key "votes_comments", "comments"
  add_foreign_key "votes_comments", "users"
end
