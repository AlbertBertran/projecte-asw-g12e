class RemoveUserIdToContributions < ActiveRecord::Migration[6.1]
  def change
    remove_column :contributions, :user_id, :integer
  end
end
