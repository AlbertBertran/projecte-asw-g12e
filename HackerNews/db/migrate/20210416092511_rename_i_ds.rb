class RenameIDs < ActiveRecord::Migration[6.1]
  def change
    rename_column :comments, :contribution, :contribution_id
    rename_column :comments, :author, :user_id
  end
end
