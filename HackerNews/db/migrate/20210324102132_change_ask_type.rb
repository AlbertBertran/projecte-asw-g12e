class ChangeAskType < ActiveRecord::Migration[6.1]
  def change
    remove_column :contributions, :ask, :string
    add_column :contributions, :ask, :text
  end
end
