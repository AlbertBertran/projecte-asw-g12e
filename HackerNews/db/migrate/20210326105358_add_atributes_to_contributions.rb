class AddAtributesToContributions < ActiveRecord::Migration[6.1]
  def change
    add_column :contributions, :points, :integer
    add_column :contributions, :type_url, :boolean
  end
end
