class AddCommentRefToVotes < ActiveRecord::Migration[6.1]
  def change
    add_reference :votes, :comment, foreign_key: true
  end
end
