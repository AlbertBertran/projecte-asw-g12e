class AddContributionRefToVotes < ActiveRecord::Migration[6.1]
  def change
    add_reference :votes, :contribution, null: false, foreign_key: true
  end
end
