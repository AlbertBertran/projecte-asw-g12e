class AddApiKeyToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :APIKey, :string
    add_index :users, :APIKey, unique: true
  end
end
