class CreateContributions < ActiveRecord::Migration[6.1]
  def change
    create_table :contributions do |t|
      t.string :title
      t.string :url
      t.string :ask
      t.integer :user_id

      t.timestamps
    end
  end
end
