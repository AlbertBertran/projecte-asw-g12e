class CreateComments < ActiveRecord::Migration[6.1]
  def change
    create_table :comments do |t|
      t.integer :author
      t.text :text
      t.integer :contribution

      t.timestamps
    end
  end
end
