class CommentsController < ApplicationController
  before_action :set_comment, only: %i[ show edit update destroy vote unvote]

  # GET /comments or /comments.json
  def index
    @comments = Comment.all
  end

  # GET /comments/1 or /comments/1.json
  def show
  end

  # GET /comments/new
  def new
    @contribution = Contribution.find(params[:contribution_id])
    @comment = @contribution.comments.new(parent_id: params[:parent_id])
  end

  # GET /comments/1/edit
  def edit
  end
  
  #GET /comments_user or /comments_user.json
  def comments_user
    respond_to do |format|
      format.html{render :comments_user}
      format.json{
        if params.has_key?(:upvoted)
          @current_user = User.find_by(:APIKey => request.headers["X-API-KEY"])
          if @current_user.nil? or request.headers["X-API-KEY"].nil?
            render :json => {message: "Please insert a valid API Key" }, status: :unauthorized
          else
            @votes = VotesComment.where(user_id: User.find_by(id: @current_user.id))
            @comments = Comment.none
            @votes.each do |vote|
              @comments = @comments + Comment.where(id: vote.comment_id)
            end
            render json: render_to_string('index.json.jbuilder', locals: { comments: @comments })
          end
        else
          @comments = Comment.where(:user_id => params[:user_id])
          render json: render_to_string('index.json.jbuilder', locals: { comments: @comments })
        end
      }
    end
  end
  
   #PUT /vote
  def vote
    @vote = VotesComment.new
    @vote.comment_id = @comment.id
    if request.format.json?
      @current_user = User.find_by(:APIKey => request.headers["X-API-KEY"])
      has_voted  =  VotesComment.find_by(user_id: @current_user.id, comment_id: @comment.id)!= nil
      can_vote = @comment.user_id != @current_user.id
      if has_voted or !can_vote
        render json: {message: "You cannot vote this comment"}, status: :unauthorized
        return
      end
      @vote.user_id = @current_user.id
    else
      @vote.user_id = current_user.id
    end
    respond_to do |format|
      if @vote.save
        @comment.points = @comment.points + 1 
        @comment.save
        format.html { redirect_back(fallback_location: root_path) }
        format.json { render json: {message: "Comment voted successfully"} }
      else
        format.html { redirect_back fallback_location: @comment }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  #PUT /unvote
  def unvote
    if request.format.json?
      @current_user = User.find_by(:APIKey => request.headers["X-API-KEY"])
      @vote = VotesComment.find_by(user_id: @current_user.id, comment_id: @comment.id)
      has_voted  =  @vote != nil
      can_vote = @comment.user_id != @current_user.id
      if !has_voted or !can_vote
        render json: {message: "You cannot unvote this comment"}, status: :unauthorized
        return
      end
    else
      @vote =  VotesComment.find_by(user_id: User.find_by(id: session[:user_id]).id, comment_id: @comment.id)
    end
    respond_to do |format|
      if @vote.destroy
        @comment.points = @comment.points - 1 
        @comment.save
        format.html { redirect_back(fallback_location: root_path) }
        format.json { render json: {message: "Comment unvoted successfully"} }
      else
        format.html { redirect_back fallback_location: @comment }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /comments or /comments.json
  def create
    #@contribution = Contribution.find(params[:contribution_id])
    #@comment = @contribution.comments.new(comment_params)
    @comment = Comment.new(comment_params)
    @comment.points = 0
    if request.format.json?
      if @comment.contribution_id == nil or Contribution.find_by(id: @comment.contribution_id) == nil
        render :json => {message: "Please insert a valid contribution id" }, status: :unauthorized
        return
      end
      @current_user = User.find_by(:APIKey => request.headers["X-API-KEY"])
      if @current_user.nil? or request.headers["X-API-KEY"].nil?
        render :json => {message: "Please insert a valid API Key" }, status: :unauthorized
        return
      else
        @comment.user_id = @current_user.id
        if(params[:parent_id] != nil)
          @parent = Comment.find_by(id: params[:parent_id])
          if(@parent.contribution_id != @comment.contribution_id)
            render :json => {message: "The parent does not belong to the contribution" }, status: :unauthorized
            return
          else
            @comment.parent_id = params[:parent_id]
          end
        end
      end
    end
    respond_to do |format|
      if @comment.save
        format.html { redirect_to controller: 'contributions' , action: 'show', id: @comment.contribution_id}
        format.json { render :show, status: :created, location: @comment }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comments/1 or /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment, notice: "Comment was successfully updated." }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1 or /comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to Contribution.find_by(:id => @comment.contribution_id) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def comment_params
      params.require(:comment).permit(:user_id, :text, :contribution_id, :parent_id, :points)
    end
end
