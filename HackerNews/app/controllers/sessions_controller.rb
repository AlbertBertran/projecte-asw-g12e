class SessionsController < ApplicationController

  # Handle Google OAuth 2.0 login callback.
  #
  # GET /auth/google_oauth2/callback
  def create
    
    user = User.form_omniauth(request.env['omniauth.auth'])
    if user.valid?
        session[:user_id] = user.id
        token = rand(64**8).to_s(16)
            while User.find_by(:APIKey => token) do
              token = rand(64**8).to_s(16)
            end
            if user.update_attribute(:APIKey, token)
              redirect_to root_path
            else
              render json: user.errors, status: :unprocessable_entity
            end
    end 
  end
  
  def destroy
    session.delete(:user_id)
    @current_user = nil
    redirect_to root_url
  end
end