class ContributionsController < ApplicationController
  before_action :set_contribution, only: %i[ show edit update destroy vote unvote]
  # GET /contributions or /contributions.json
  def index
    @contributions = Contribution.all
  end
  
  def newest
    @contributions = Contribution.all.order(created_at: :desc)
    render "contributions/index"
  end

  # GET /contributions/1 or /contributions/1.json
  def show
    @comment = Comment.new
    respond_to do |format|
      format.html{render :show, location: @contribution}
      format.json{
        @comments = Comment.where(contribution_id: @contribution.id)
        render json: render_to_string('show.json.jbuilder', locals: { contribution: @contribution, comments: @comments })
      }
    end
  end

  # GET /ask
  def ask
    @contributions =  Contribution.where(type_url: false);
    respond_to do |format|
      format.html {render :ask, status: :unprocessable_entity }
      format.json { render json: @contributions}
    end
  end
  
  # GET /contributions/new
  def new
    @contribution = Contribution.new
    
  end

  # GET /contributions/1/edit
  def edit
  end
  
  #GET /contributions_user or /contributions_user.json
  def contributions_user
    respond_to do |format|
      format.html{render :contributions_user}
      format.json{
        if params.has_key?(:upvoted)
          @current_user = User.find_by(:APIKey => request.headers["X-API-KEY"])
          if @current_user.nil? or request.headers["X-API-KEY"].nil?
            render :json => {message: "Please insert a valid API Key" }, status: :unauthorized
          else
            @votes = Vote.where(user_id: User.find_by(id: @current_user.id))
            @contributions = Contribution.none
            @votes.each do |vote|
              @contributions = @contributions + Contribution.where(id: vote.contribution_id)
            end
            render json: render_to_string('index.json.jbuilder', locals: { contributions: @contributions })
          end
        else
          @contributions = Contribution.where(:user_id => params[:user_id])
          render json: render_to_string('index.json.jbuilder', locals: { contributions: @contributions })
        end
      }
    end
  end
  
  #PUT /vote
  def vote
    @vote = Vote.new
    @vote.contribution_id = @contribution.id
    if request.format.json?
      @current_user = User.find_by(:APIKey => request.headers["X-API-KEY"])
      has_voted  =  Vote.find_by(user_id: @current_user.id, contribution_id: @contribution.id)!= nil
      can_vote = @contribution.user_id != @current_user.id
      if has_voted or !can_vote
        render json: {message: "You cannot vote this contribution"}, status: :unauthorized
        return
      end
      @vote.user_id = @current_user.id
    else
      @vote.user_id = current_user.id
    end
    respond_to do |format|
      if @vote.save
        @contribution.points = @contribution.points + 1 
        @contribution.save
        format.html { redirect_back(fallback_location: root_path) }
        format.json { render json: {message: "Voted successfully"} }
      else
        format.html { redirect_back fallback_location: @contribution }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
      end
    end
  end
  
    
  #PUT /unvote
  def unvote
    if request.format.json?
      @current_user = User.find_by(:APIKey => request.headers["X-API-KEY"])
      @vote =  Vote.find_by(user_id: @current_user.id, contribution_id: @contribution.id)
      has_voted = @vote != nil
      can_vote = @contribution.user_id != @current_user.id
      if !has_voted or !can_vote
        render json: {message: "You cannot unvote this contribution"}, status: :unauthorized
        return
      end
    else
      @vote =  Vote.find_by(user_id: User.find_by(id: session[:user_id]).id, contribution_id: @contribution.id)
    end
    respond_to do |format|
      if @vote.destroy
        @contribution.points = @contribution.points - 1 
        @contribution.save
        format.html { redirect_back(fallback_location: root_path) }
        format.json { render json: {message: "Unvoted successfully"} }
      else
        format.html { redirect_back fallback_location: @contribution }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /contributions or /contributions.json
  def create
    #@Contribution.user_id = 1
    @contribution = Contribution.new(contribution_params)
    
    if request.format.json?
      @current_user = User.find_by(:APIKey => request.headers["X-API-KEY"])
      if @current_user.nil? or request.headers["X-API-KEY"].nil?
        render :json => {message: "Please insert a valid API Key" }, status: :unauthorized
        return
      elsif @contribution.url.empty? and @contribution.ask.empty?
          render :json => {message: "Please enter either an URL or a message" }, status: :unprocessable_entity
          return
      else
        @contribution.user_id = @current_user.id
      end
    else
      @contribution.user_id= current_user.id
    end
    
    @contribution.points = 0
    if @contribution.url.empty?
        @contribution.type_url = false
    else @contribution.type_url = true
    end
    
    if @contribution.type_url
      @repeated = Contribution.find_by(:url => @contribution.url)
    else @repeated = nil
    end
    
    respond_to do |format|
      if @repeated != nil
        format.html { redirect_to contribution_path(@repeated) }
        format.json {
          render :json => {message: "This url has already been submited in contribution: " + @repeated.id.to_s}, status: :unauthorized
          #@comments = Comment.where(contribution_id: @repeated.id)
          #render json: render_to_string('show.json.jbuilder', locals: { contribution: @repeated, comments: @comments })
          #render :show, status: :ok, location: @repeated
          return
        }
      else if @contribution.save
        if @contribution.type_url && !@contribution.ask.empty?
          @comment = Comment.create({:user_id => @contribution.user_id, :text => @contribution.ask, :contribution_id => @contribution.id, :parent_id => nil, :points => 0})
        end
        format.html { redirect_to contributions_path }
        format.json { render :show, status: :created, location: @contribution }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
      end
      end
    end
  end

  # PATCH/PUT /contributions/1 or /contributions/1.json
  def update
    respond_to do |format|
      if @contribution.update(contribution_params)
        format.html { redirect_to @contribution, notice: "Contribution was successfully updated." }
        format.json { render :show, status: :ok, location: @contribution }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contributions/1 or /contributions/1.json
  def destroy
    @contribution.destroy
    respond_to do |format|
      format.html { redirect_to contributions_url, notice: "Contribution was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contribution
      @contribution = Contribution.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def contribution_params
      params.require(:contribution).permit(:title, :url, :ask, :user_id, :type_url)
    end
end
