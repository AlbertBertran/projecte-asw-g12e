class ApplicationController < ActionController::Base
  protect_from_forgery unless: -> { request.format.json? }
  helper_method :logged_in?, :current_user

  def logged_in?
    !current_user.nil? 
  end

  def current_user
    if request.format.json?
      @current_user = User.find_by(:APIKey => request.headers["X-API-KEY"])
      if @current_user.nil? or request.headers["X-API-KEY"].nil?
        render :json => {message: "Please insert a valid API Key" }, status: :unauthorized
      end
    else
      @current_user = User.find_by(id: session[:user_id])
    end
  end
end
