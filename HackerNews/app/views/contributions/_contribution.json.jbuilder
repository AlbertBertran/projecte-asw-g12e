json.extract! contribution, :id, :title, :url, :ask, :points, :created_at
json.user do
  json.id contribution.user.id
  json.username contribution.user.name
  json.email contribution.user.email
  json.about contribution.user.about
end
