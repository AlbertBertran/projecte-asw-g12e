json.extract! comment, :id,  :text, :points, :contribution_id, :parent_id, :created_at
json.user do
  json.id comment.user.id
  json.username comment.user.name
  json.email comment.user.email
  json.about comment.user.about
end