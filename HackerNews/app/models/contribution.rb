class Contribution < ApplicationRecord
  belongs_to :user
  has_many :comments, :dependent => :delete_all
  #if :type_url == true
    #validates :url, url: :url
  #end
end
