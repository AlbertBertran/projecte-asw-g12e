class User < ApplicationRecord
    
    validates :APIKey, uniqueness: true
    
    def self.form_omniauth(response)
        User.find_or_create_by(email: response["info"]["email"]) do |u|
            u.name = response["info"]["name"]
            u.email = response["info"]["email"]
        end
    end
    
end
